import 'package:flutter/material.dart';
import 'package:flutter_provider/CounterState.dart';
import 'package:provider/provider.dart';

void main() => runApp(ChangeNotifierProvider(
      create: (_) => CounterState(),
      child: MyApp(),
    ));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final data = Provider.of<CounterState>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Provider'),
      ),
      body: Column(
        children: <Widget>[
          Center(child: Text('${data.getDisplayText}')),
          RaisedButton(onPressed: () {
            data.setDisplayText('Ihwan');
          }),
          RaisedButton(onPressed: () {
            data.setDisplayText('Andi');
          }),
          RaisedButton(onPressed: () {
            data.setDisplayText('Rido');
          }),
          RaisedButton(onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => SecondPage()));
          })
        ],
      ),
    );
  }
}

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final myText = Provider.of<CounterState>(context);
    return Scaffold(
      body: SafeArea(child: Text('${myText.getDisplayText}')),
    );
  }
}
