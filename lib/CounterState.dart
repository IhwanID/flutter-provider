import 'package:flutter/material.dart';

class CounterState with ChangeNotifier {
  CounterState();
  String _displayText = "kosong";

  void setDisplayText(String text) {
    _displayText = text;
    notifyListeners();
  }

  String get getDisplayText => _displayText;
}
